#!/bin/bash


f_title() {
  echo
  echo "### $@"
}


CONF_TYPE=$1

if [ "$CONF_TYPE" == 'bash' ]; then
  f_title "BASH :: Copying config files"
  cp -vi defaults/bashrc ~/.bashrc
  cp -vi defaults/bash_prompt ~/.bash_prompt
  cp -vi defaults/bash_alias ~/.bash_alias
  source ~/.bashrc

elif [ "$CONF_TYPE" == 'git' ]; then
  f_title "GIT :: Copying config files"
  cp -vi defaults/gitconfig ~/.gitconfig

elif [ "$CONF_TYPE" == 'vim' ]; then
  f_title "VIM :: Copying config files"
  mkdir ~/.vim 2> /dev/null
  cp -vi defaults/vim/vimrc ~/.vimrc
  cp -vR defaults/vim/colors ~/.vim/
  f_title "VIM :: Installing Plugins"
  vim +'PlugClean --sync' +qa &> /dev/null
  vim +'PlugInstall --sync' +qa  &> /dev/null

elif [ "$CONF_TYPE" == 'nvim' ]; then
  f_title "NVIM :: Installing vim-plug"
  curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  f_title "NVIM :: Copying config files"
  mkdir -p ~/.config/nvim/ 2> /dev/null
  cp -vi defaults/nvim/init.vim ~/.config/nvim/
  cp -vR defaults/vim/colors ~/.config/nvim/
  f_title "NVIM :: Installing Plugins"
  nvim +'PlugClean --sync' +qa &> /dev/null
  nvim +'PlugInstall --sync' +qa  &> /dev/null

elif [ "$CONF_TYPE" == 'fix-ansible-vim' ]; then
  if [ $2 == 'nvim' ]; then
    PLUG_DIR=~/.local/share/nvim/plugged/ansible-vim
  else
    PLUG_DIR=~/.vim/plugged/ansible-vim
  fi
  f_title "VIM :: Fix ansible-vim loop highlight and Indentation"
  sed -i 's/^setlocal indentkeys.*/setlocal indentkeys=!^F,o,O,0#,0},0] ",<:>,-,*<Return>/g' \
    ${PLUG_DIR}/indent/ansible.vim && echo ${PLUG_DIR}/indent -- OK
  sed -i 's/with_.+\"/with_.+\|loop.+\"/g' \
    ${PLUG_DIR}/syntax/ansible.vim && echo ${PLUG_DIR}/syntax -- OK 

elif [ "$CONF_TYPE" == 'termux' ]; then
  f_title "TERMUX :: Copying config files"
  mkdir ~/.termux 2> /dev/null
  cp extras/termux/* ~/.termux

elif [ "$CONF_TYPE" == 'msys' ]; then
  f_title "MSYS :: Installing packages"
  bash extras/msys2/setup_msys.sh

elif [ "$CONF_TYPE" == 'dev' ]; then
  bash $0 bash
  bash $0 git
  bash $0 vim
  bash $0 fix-ansible-vim

else
  f_title "Argument error! "
  echo '### Valid args: bash|git|vim|nvim|termux|msys'
  echo '                fix-ansible-vim|dev(bash+git+vim)'
  echo "Ex.: $0 dev"

fi
